import { html, fixture, assert, fixtureCleanup } from '@open-wc/testing';
import '../cells-combobox.js';

suite('CellsCombobox', () => {
  let el;

  teardown(() => fixtureCleanup());

  setup(async () => {
    el = await fixture(html`<cells-combobox></cells-combobox>`);
    await el.updateComplete;
  });

  test('Instantiating component', () => {
    const maskSelect = el.shadowRoot.querySelector('.mask-select');
    assert.isNotNull(maskSelect);
  });

  test('Test props empty and false', () => {
    let valueTest = '';
    el.errorMessage = valueTest;
    el.invalid = false;
    el.disabled = false;
    el.readonly = false;
  });

  test('Test props value and true', () => {
    let valueTest = 'test';
    el.errorMessage = valueTest;
    el.invalid = true;
    el.disabled = true;
    el.readonly = true;
  });

  test('Test event OnClickWindow', () => {
    const click = new CustomEvent('click', {
      detail: true,
      bubbles: true,
      composed: true,
    });
    window.dispatchEvent(click);
  });

  test('Test event hideListAll', () => {
    el.idSelect = 'id';
    const hideListAll = new CustomEvent('hide-list-all', {
      detail: { id: 'id' },
      bubbles: true,
      composed: true,
    });
    window.dispatchEvent(hideListAll);
  });

  test('Test event onKeyDown', () => {
    el.idSelect = 'id';
    const keydown = new CustomEvent('keydown', {
      detail: { id: 'id' },
      bubbles: true,
      composed: true,
    });
    window.dispatchEvent(keydown);
  });

  test('Test event onKeyUp', () => {
    const keyup = new CustomEvent('keyup', {
      detail: true,
      bubbles: true,
      composed: true,
    });
    el.shadowRoot.querySelector('bbva-form-field').dispatchEvent(keyup);
    let e = {
      keyCode: 40,
    };
    el.onKeyUp(e);
  });

  test('Test function clearItems', () => {
    el.clearItems();
    assert.isTrue(el.items.length === 0);
  });

  test('Test function clearSelected', () => {
    el.clearSelected();
    assert.isEmpty(el.value);
    assert.isNull(el.selectedItem);
  });

  test('Test function set and get selected', () => {
    el.setSelected(
      {
        text: 'Value',
        value: '1',
      },
      'Value'
    );
    assert.isNotEmpty(el.value);
    assert.isNotNull(el.selectedItem);
    assert.isNotNull(el.getSelected());
  });

  test('Test function onKeyDown(keyCode = 38)', () => {
    let e = {
      keyCode: 38,
    };
    el.shadowRoot
      .querySelector('.list-options')
      .classList.add('list-options-show');
    el.onKeyDown(e);
  });

  test('Test function onKeyDown(keyCode = 40)', () => {
    let e = {
      keyCode: 40,
    };
    el.shadowRoot
      .querySelector('.list-options')
      .classList.add('list-options-show');
    el.onKeyDown(e);
  });

  test('Test function onKeyDown(keyCode = 13)', () => {
    let e = {
      keyCode: 13,
    };
    el.shadowRoot.querySelector('div').classList.add('hover');
    el.shadowRoot.querySelector('div').setAttribute('data-item', '{}');
    el.shadowRoot
      .querySelector('.list-options')
      .classList.add('list-options-show');
    el.onKeyDown(e);
  });

  test('Test function onKeyDown(keyCode = defaulf)', () => {
    let e = {
      keyCode: 0,
    };
    el.shadowRoot
      .querySelector('.list-options')
      .classList.add('list-options-show');
    el.onKeyDown(e);
  });

  test('Test function _selectorKeyDown(up)', () => {
    let item = el.shadowRoot.querySelector('div');
    item.classList.add('hide');
    el.itemHover = item;
    el._selectorKeyDown('up', false);
  });

  test('Test function _selectorKeyDown(down)', () => {
    let item = el.shadowRoot.querySelector('div');
    item.classList.add('hide');
    el.itemHover = item;
    el._selectorKeyDown('down', false);
  });

  test('Test function _removeAllHovers', () => {
    let item = el.shadowRoot.querySelector('div');
    item.classList.add('hover');
    el._removeAllHovers();
  });

  test('Test function activeSelected', () => {
    el.selectedItem = { pathId: 'value' };
    el.pathId = 'pathId'
    el.shadowRoot.querySelector('div').classList.add('option');
    el.shadowRoot.querySelector('div').setAttribute('data-item', JSON.stringify({ pathId: 'value' }));
    el.activeSelected();
  });

  test('Test function showList disabled or readOnly', () => {
    el.disabled = true;
    el.readonly = true;
    el.showList(null);
  });

  test('Test function showList enabled', () => {
    let evt = {
      stopPropagation: function(){}
    }
    el.shadowRoot.querySelector('.list-options').classList.add('list-options-show');
    el.showList(evt);

    el.shadowRoot.querySelector('.list-options').classList.remove('list-options-show');
    el.showList(evt);

  });

  test('Test function _removeAllActives', () => {
    let item = el.shadowRoot.querySelector('div');
    item.classList.add('option');
    el._removeAllActives();
  });

  test('Test function selectedItemList notEmpty', () => {
    const item = { value: 'value' };
    el.selectedItemList(item);
  });

  test('Test function getLabel', () => {
    el.pathText = 'value';
    const item = { value: 'text' };
    assert.equal(el.getLabel(item), item.value);
  });

  test('Test function _removeAllHide', () => {
    let item = el.shadowRoot.querySelector('div');
    item.classList.add('option');
    el._removeAllHide();
  });

  test('Test function filterItemsList', () => {
    let item = el.shadowRoot.querySelector('div');
    item.classList.add('option');
    el.filterItemsList();
  });

  test('Test function filterItemsList whit query value match ok', () => {
    let item = el.shadowRoot.querySelector('div');
    item.classList.add('option');
    el.pathText = 'value';
    item.setAttribute('data-item', '{"value": "thisQuery"}');
    el.shadowRoot.querySelector('.input-search').value = 'query';
    el.filterItemsList();
  });

  test('Test function filterItemsList whit query value match none', () => {
    let item = el.shadowRoot.querySelector('div');
    item.classList.add('option');
    el.pathText = 'value';
    item.setAttribute('data-item', '{"value": "nonMatch"}');
    el.shadowRoot.querySelector('.input-search').value = 'query';
    el.filterItemsList();
  });
  

});
