import { html } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import { BaseElement } from '@bbva-commons-web-components/cells-base-elements';
import styles from './CellsCombobox-styles.js';
import '@cells-components/coronita-icons/coronita-icons.js';
import '@cells-components/cells-icon/cells-icon.js';
import '@bbva-web-components/bbva-form-field';
/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<cells-combobox></cells-combobox>
```

##styling-doc

@customElement cells-combobox
*/
export class CellsCombobox extends BaseElement {
  static get is() {
    return 'cells-combobox';
  }

  // Declare properties
  static get properties() {
    return {
      label: String,
      value: String,
      name: String,
      items: Array,
      filter: Boolean,
      pathRoot: String,
      pathText: String,
      pathId: String,
      selectedItem: Object,
      openIcon: String,
      closeIcon: String,
      iconSize: String,
      idSelect: String,
      eventChangeSelect: String,
      itemHover: Object,
      errorMessage: String,
      invalid: Boolean,
      hostService: String,
      pathService: String,
      eventLoad: String,
      disabled: Boolean,
      readonly: Boolean,
      addTextAll: String,
      noLoading: Boolean,
      selectMaskLoading: Boolean,
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.selectMaskLoading = false;
    this._initialize();
  }

  onclickWindow() {
    if (this.element('.icon-expand')) {
      this.element('.icon-expand').icon = this.openIcon;
    }
    if (this.element('.mask-select')) {
      this.element('.mask-select').classList.remove('mask-select-show');
    }
    if (this.element('.list-options')) {
      this.element('.list-options').classList.remove('list-options-show');
    }
  }

  hideListAll(e) {
    if (e.detail.id !== this.idSelect) {
      if (this.element('.list-options') && this.element('.icon-expand')) {
        this.element('.list-options').classList.remove('list-options-show');
        this.element('.icon-expand').icon = this.openIcon;
      }
    }
  }

  onKeyUp(e) {
    const key = e.keyCode ? e.keyCode : e.which;
    if (
      key === 40 &&
      !this.element('.list-options').classList.contains('list-options-show')
    ) {
      this.activeList();
      this.element('.icon-expand').icon = this.closeIcon;
    }
  }

  async _initialize() {
    this.filter = true;
    this.idSelect = `form-select-${Math.floor(1 + Math.random() * 999999999)}`;
    this.items = [];
    this.filterItems = [];
    this.pathId = 'value';
    this.pathText = 'text';
    this.openIcon = 'coronita:unfold';
    this.closeIcon = 'coronita:fold';
    this.iconSize = 24;
    this.eventChangeSelect = 'option-selected';
    window.addEventListener('click', () => {
      this.onclickWindow();
    });
    window.addEventListener('hide-list-all', (e) => {
      this.hideListAll(e);
    });

    window.addEventListener('keydown', (e) => this.onKeyDown(e));

    await this.updateComplete;
    this.element('bbva-form-field').addEventListener('keyup', (e) =>
      this.onKeyUp(e)
    );
  }

  clearItems() {
    this.items = [];
    this.requestUpdate();
  }

  clearSelected() {
    this.value = '';
    this.selectedItem = null;
    this.requestUpdate();
  }

  getSelected() {
    return this.selectedItem;
  }

  setSelected(item, value) {
    this.selectedItem = item;
    this.value = value;
    this.requestUpdate();
  }

  onKeyDown(e) {
    if (this.elementsAll('.list-options-show').length === 0) {
      return;
    }
    const key = e.keyCode ? e.keyCode : e.which;
    let hoverActive;
    switch (key) {
      case 38:
        this._selectorKeyDown('up');
        break;
      case 40:
        this._selectorKeyDown('down');
        break;
      case 13:
        hoverActive = this.element('.hover');
        if (hoverActive && hoverActive.dataset && hoverActive.dataset.item) {
          const item = JSON.parse(hoverActive.dataset.item);
          this.selectedItemList(item);
        }
        break;
      default:
        break;
    }
  }

  _selectorKeyDown(type, first) {
    let isFirst = false;

    if (!this.itemHover) {
      isFirst = true;
      const [item] = this.elementsAll('.option:not(.hide)');
      this.itemHover = item;
      this.requestUpdate();
    }

    if(typeof first !== 'undefined') {
      isFirst = first;
    }

    const tempItemHover = this.itemHover;

    if (!isFirst) {
      if (type === 'up') {
        this.itemHover = this.itemHover.previousElementSibling;
      } else if (type === 'down') {
        this.itemHover = this.itemHover.nextElementSibling;
      }
      this.requestUpdate();
    }

    while (this.itemHover && this.itemHover.classList.contains('hide')) {
      if (type === 'up') {
        this.itemHover = this.itemHover.previousElementSibling;
      } else if (type === 'down') {
        this.itemHover = this.itemHover.nextElementSibling;
      }
      this.requestUpdate();
    }
    if (this.itemHover) {
      this._removeAllHovers();
      this.itemHover.classList.add('hover');
    } else {
      this.itemHover = tempItemHover;
    }
    this.requestUpdate();
  }

  _removeAllHovers() {
    const hovers = this.elementsAll('.hover');
    hovers.forEach((h) => {
      h.classList.remove('hover');
    });
  }

  updated(changedProperties) {
    changedProperties.forEach((oldValue, propName) => {
      if (propName === 'value') {
        if (this.isEmpty(this.value)) {
          this.element('bbva-form-field').removeAttribute('value');
        } else {
          this.element('bbva-form-field').setAttribute('value', this.value);
        }
      } else if (propName === 'errorMessage') {
        if (this.isEmpty(this.errorMessage)) {
          this.element('bbva-form-field').removeAttribute('errorMessage');
        } else {
          this.element('bbva-form-field').setAttribute(
            'error-message',
            this.errorMessage
          );
        }
      } else if (propName === 'invalid') {
        if (!this.invalid) {
          this.element('bbva-form-field').classList.remove('invalid');
          this.element('bbva-form-field').removeAttribute('invalid');
        } else {
          this.element('bbva-form-field').classList.add('invalid');
          this.element('bbva-form-field').setAttribute('invalid', true);
        }
      } else if (propName === 'disabled') {
        this.element('.icon-expand').icon = this.openIcon;
        if (!this.disabled) {
          this.element('.icon-expand').classList.remove('hide');
          this.element('bbva-form-field').removeAttribute('disabled');
        } else {
          this.element('.icon-expand').classList.add('hide');
          this.element('bbva-form-field').setAttribute('disabled', 'disabled');
        }
      } else if (propName === 'readonly') {
        this.element('.icon-expand').icon = this.openIcon;
        if (!this.readonly) {
          this.element('.icon-expand').classList.remove('hide');
        } else {
          this.element('.icon-expand').classList.add('hide');
        }
      }
    });
  }

  clearFilter() {
    this.itemHover = null;
    this._removeAllHovers();
    this._removeAllActives();
    this.element('.input-search').value = '';
    this._removeAllHide();
  }

  activeSelected() {
    if (!this.isEmpty(this.selectedItem)) {
      const items = this.elementsAll('.option');
      items.forEach((item) => {
        if (item.dataset && item.dataset.item) {
          const element = JSON.parse(item.dataset.item);
          if (
            this.extract(this.selectedItem, this.pathId, '') ===
            this.extract(element, this.pathId, '--')
          ) {
            item.classList.add('active');
          }
        }
      });
    }
  }

  activeList() {
    this._removeAllHovers();
    this.clearFilter();
    this.activeSelected();
    this.dispatch('hide-list-all', { id: this.idSelect });
    this.element('.list-options').classList.toggle('list-options-show');
  }

  showList(e) {
    if (this.disabled || this.readonly) {
      return;
    }
    e.stopPropagation();
    if (!this.element('.list-options').classList.contains('list-options-show')) {
      this.element('.icon-expand').icon = this.closeIcon;
    } else {
      this.element('.icon-expand').icon = this.openIcon;
    }
    this.activeList();
    this.element('.mask-select').classList.add('mask-select-show');
  }

  _removeAllActives() {
    this.elementsAll('.option').forEach((element) => {
      element.classList.remove('active');
    });
  }

  selectedItemList(item) {
    this._removeAllActives();
    this.selectedItem = item;
    if (this.isNotEmpty(item)) {
      this.invalid = false;
    }
    this.element('.mask-select').classList.remove('mask-select-show');
    this.element('.list-options').classList.remove('list-options-show');
    this.element('.icon-expand').icon = this.openIcon;
    this.value = this.extract(item, this.pathText, '');
    this.requestUpdate();
    this.dispatch(this.eventChangeSelect, item);
  }

  getLabel(item) {
    return this.extract(item, this.pathText, '');
  }

  _removeAllHide() {
    const options = this.elementsAll('.option');
    options.forEach((option) => {
      option.classList.remove('hide');
    });
  }

  filterItemsList() {
    this.itemHover = null;
    this._removeAllHovers();
    this._removeAllActives();
    const query = this.element('.input-search').value;
    const options = this.elementsAll('.option');
    if (this.isEmpty(query)) {
      options.forEach((option) => {
        option.classList.remove('hide');
      });
    } else {
      options.forEach((option) => {
        const item = JSON.parse(option.dataset.item);
        if (
          item[this.pathText].toUpperCase().indexOf(query.toUpperCase()) >= 0
        ) {
          option.classList.remove('hide');
        } else {
          option.classList.add('hide');
        }
      });
    }
    this.activeSelected();
  }

  static get styles() {
    return [styles, getComponentSharedStyles('cells-combobox-shared-styles')];
  }

  // Define a template
  render() {
    return html`
      <slot></slot>

      <div class="mask-select"></div>
      <div class="container-input">

      <div ?hidden = "${!this.selectMaskLoading}" class = "select-mask-loading" >
        <div  class="lds-css ng-scope"><div class="lds-rolling"><div></div></div></div>
      </div>

        <cells-icon
          icon="${this.openIcon}"
          size="24"
          class="icon-expand"
          @click=${this.showList}
        ></cells-icon>
        <bbva-form-field
          class="form-input"
          optional-label=""
          @click=${this.showList}
          label="${this.label}"
          readonly
        ></bbva-form-field>
        <div class="list-options">
          <input
            tabindex="-1"
            @click=${(e) => e.stopPropagation()}
            @input=${this.filterItemsList}
            placeholder="Busqueda"
            ?hidden="${!this.filter}"
            class="input-search"
          />
          <div class="list">
            ${this.items.map(
              (item, index) => html`
                <div
                  id="option-select-${index}"
                  class="option"
                  @click=${() => this.selectedItemList(item)}
                  data-item="${JSON.stringify(item)}"
                >
                  <span>${this.getLabel(item)}</span
                  ><cells-icon icon="coronita:checkmark"></cells-icon>
                </div>
              `
            )}
          </div>
        </div>
      </div>
    `;
  }
}
